package gopt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
	"strings"

	"gitlab.com/srcgo/gopt/gopt/chat"
	"gitlab.com/srcgo/gopt/gopt/collection"
	"gitlab.com/srcgo/gopt/gopt/http"
	"gitlab.com/srcgo/gopt/gopt/json"
)

const ApiKeyEnvVar = "OPENAI_API_KEY"
const OrganizationIdEnvVar = "OPENAI_ORGANIZATION_ID"

// Client provides an interface to access the OpenAI REST API.
type Client interface {
	// ListModels makes an request to the OpenAI API models endpoint and returns an
	// Iterator of the response(s).
	ListModels() collection.Iterator[ModelsResponse]

	// RetrieveModel makes an request to the OpenAI API 'model' endpoint and returns an
	// Iterator of the response(s).
	RetrieveModel(int) collection.Iterator[ModelResponse]

	// CreateCompletion makes an request to the OpenAI API 'completions' endpoint and
	// returns an Iterator of the response(s).
	CreateCompletion(string) collection.Iterator[CompletionResponse]

	// CreateCompletion makes an request to the OpenAI API 'chat/completions' endpoint
	// and returns an Iterator of the response(s).
	CreateChatCompletion([]chat.Message) collection.Iterator[ChatCompletionResponse]

	// CreateEdit makes an request to the OpenAI API 'edit' endpoint and returns an
	// Iterator of the response(s).
	CreateEdit(string) collection.Iterator[EditResponse]

	// CreateImage makes an request to the OpenAI API 'image' endpoint and returns an
	// Iterator of the response(s).
	CreateImage(string) collection.Iterator[ImageResponse]

	// CreateImageEdit makes an request to the OpenAI API 'image_edit' endpoint and
	// returns an Iterator of the response(s).
	CreateImageEdit(string) collection.Iterator[ImageEditResponse]

	// CreateImageVariation makes an request to the OpenAI API 'image_variation'
	// endpoint and returns an Iterator of the response(s).
	CreateImageVariation(string) collection.Iterator[ImageVariationResponse]

	// CreateEmbedding makes an request to the OpenAI API 'embedding' endpoint and
	// returns an Iterator of the response(s).
	CreateEmbedding(string) collection.Iterator[EmbeddingResponse]

	// WriteCompletion writes the response text of the requested completion to the
	// given Writer.
	WriteCompletion(string, io.Writer) error

	// WriteCompletion writes the response text of the requested chat completion to the
	// given Writer.
	WriteChatCompletion([]chat.Message, io.Writer) error

	// WriteEdit writes the response text of the requested edit to the given Writer.
	WriteEdit(string, io.Writer) error

	// WriteImage writes the bytes of the requested image to the given Writer.
	WriteImage(string, io.Writer) error

	// WriteImageEdit writes the bytes of the requested image edit to the given Writer.
	WriteImageEdit(string, io.Writer) error

	// WriteImageVariation writes the bytes of the requested image variation to the
	// given Writer.
	WriteImageVariation(string, io.Writer) error
}

type client struct {
	orgId  string
	apiKey func() string
	config Config
}

// NewClient instantiates a new client with the default configuration.
func NewClient(apiKey, orgId string) Client {
	return NewClientConfigured(apiKey, orgId, DefaultConfig)
}

// NewClientConfigured instantiates a new client with the given config.
func NewClientConfigured(apiKey, orgId string, config Config) Client {
	cl := client{orgId: orgId, config: config}
	hash := make([]byte, 32)
	rand.Read(hash)

	ciph := must(aes.NewCipher(hash))
	gcm := must(cipher.NewGCM(ciph))
	nonceSize := gcm.NonceSize()
	nonce := make([]byte, nonceSize)
	rand.Read(nonce)

	cipher := gcm.Seal(nil, nonce, []byte(apiKey), nil)

	cl.apiKey = func() string {
		plain := must(gcm.Open(nil, nonce, cipher, nil))
		return string(plain)
	}

	return &cl
}

func (cl *client) ListModels() collection.Iterator[ModelsResponse] {
	return do[ModelsResponse](
		cl, "GET", "https://api.openai.com/v1/models",
		cl.config.Models.EOS,
		cl.config.Models.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.Models.Body)
}

func (cl *client) RetrieveModel(id int) collection.Iterator[ModelResponse] {
	return do[ModelResponse](
		cl, "GET", fmt.Sprintf("https://api.openai.com/v1/models/%d", id),
		cl.config.Model.EOS,
		cl.config.Model.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.Models.Body)
}

func (cl *client) CreateCompletion(input string) collection.Iterator[CompletionResponse] {
	return do[CompletionResponse](
		cl, "POST", "https://api.openai.com/v1/completions",
		cl.config.Completion.EOS,
		cl.config.Completion.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.Completion.BodyWith(map[string]any{
			"prompt":     input,
			"max_tokens": 4096 - EstimatedTokenCount(input),
		}))
}

func (cl *client) CreateChatCompletion(input []chat.Message) collection.Iterator[ChatCompletionResponse] {
	return do[ChatCompletionResponse](
		cl, "POST", "https://api.openai.com/v1/chat/completions",
		cl.config.ChatCompletion.EOS,
		cl.config.ChatCompletion.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.ChatCompletion.BodyWith(map[string]any{
			"messages": input,
		}))
}

func (cl *client) CreateEdit(input string) collection.Iterator[EditResponse] {
	return do[EditResponse](
		cl, "POST", "https://api.openai.com/v1/edits",
		cl.config.Edit.EOS,
		cl.config.Edit.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.Edit.BodyWith(map[string]any{
			"prompt":     input,
			"max_tokens": 4096 - EstimatedTokenCount(input),
		}))
}

func (cl *client) CreateImage(input string) collection.Iterator[ImageResponse] {
	return do[ImageResponse](
		cl, "POST", "https://api.openai.com/v1/images/generations",
		cl.config.Image.EOS,
		cl.config.Image.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.Image.BodyWith(map[string]any{
			"prompt": input,
		}))
}

func (cl *client) CreateImageEdit(input string) collection.Iterator[ImageEditResponse] {
	return do[ImageEditResponse](cl, "POST", "https://api.openai.com/v1/images/edits",
		cl.config.ImageEdit.EOS,
		cl.config.ImageEdit.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.ImageEdit.BodyWith(map[string]any{
			"prompt": input,
		}))
}

func (cl *client) CreateImageVariation(input string) collection.Iterator[ImageVariationResponse] {
	return do[ImageVariationResponse](cl, "POST", "https://api.openai.com/v1/images/variations",
		cl.config.ImageVariation.EOS,
		cl.config.ImageVariation.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.ImageVariation.BodyWith(map[string]any{
			"prompt": input,
		}))
}

func (cl *client) CreateEmbedding(input string) collection.Iterator[EmbeddingResponse] {
	return do[EmbeddingResponse](cl, "POST", "https://api.openai.com/v1/embeddings",
		cl.config.Embedding.EOS,
		cl.config.Embedding.HeaderWith(map[string]any{
			"Authorization": fmt.Sprintf("Bearer %s", cl.apiKey()),
			"Content-Type":  "application/json",
		}),
		cl.config.Embedding.BodyWith(map[string]any{
			"input": input,
		}))
}

func (cl *client) WriteCompletion(input string, out io.Writer) error {
	it := cl.CreateCompletion(input)

	if stream, has := cl.config.Completion.Body["stream"]; has && stream.(bool) {
		// trim of leading whitespaces (this behaviour might change!)
		var started bool

		it = it.SkipIf(-1, func(cr CompletionResponse) bool {
			if cr.Error != nil {
				return false
			}

			if !started {
				started = len(strings.TrimSpace(cr.Choices[0].Text)) > 0
			}

			return !started
		})

		for next, hasNext := it.Next(); hasNext; next, hasNext = it.Next() {
			if next.Error != nil {
				return fmt.Errorf("%s", next.Error.Message)
			}

			if _, err := out.Write([]byte(next.Choices[0].Text)); err != nil {
				return err
			}
		}
	} else {
		if next, hasNext := it.Next(); hasNext {
			if next.Error != nil {
				return fmt.Errorf("%s", next.Error.Message)
			}

			if _, err := out.Write([]byte(strings.TrimSpace(next.Choices[0].Text))); err != nil {
				return err
			}
		}
	}

	if err := it.Error(); err != nil {
		return err
	}

	return nil
}

func (cl *client) WriteChatCompletion(input []chat.Message, out io.Writer) error {
	it := cl.CreateChatCompletion(input)

	if stream, has := cl.config.ChatCompletion.Body["stream"]; has && stream.(bool) {
		for next, hasNext := it.Next(); hasNext; next, hasNext = it.Next() {
			if next.Error != nil {
				return fmt.Errorf("%s", next.Error.Message)
			}

			if m := next.Choices[0].Delta.Content; m != nil {
				if _, err := out.Write([]byte(*m)); err != nil {
					return err
				}
			}
		}
	} else {
		if next, hasNext := it.Next(); hasNext {
			if next.Error != nil {
				return fmt.Errorf("%s", next.Error.Message)
			}

			if _, err := out.Write([]byte(strings.TrimSpace(next.Choices[0].Message.Content))); err != nil {
				return err
			}
		}
	}

	return nil
}

func (cl *client) WriteEdit(input string, out io.Writer) error {
	return fmt.Errorf("not implemented")
}

func (cl *client) WriteImage(input string, out io.Writer) error {
	return fmt.Errorf("not implemented")
}

func (cl *client) WriteImageEdit(input string, out io.Writer) error {
	return fmt.Errorf("not implemented")
}

func (cl *client) WriteImageVariation(input string, out io.Writer) error {
	return fmt.Errorf("not implemented")
}

// Performs a http request and constructs an Iterator for the result channels.
func do[T any](cl *client, method, url, eos string, header, body map[string]any) collection.Iterator[T] {
	resFrom, errFrom := http.Request(method, url, header, body)
	resTo, errTo := json.PipeTo[T](resFrom, errFrom, eos)
	return newResponseIterator(resTo, errTo)
}

func must[T any](t T, err error) T {
	if err != nil {
		panic(err.Error())
	}

	return t
}
