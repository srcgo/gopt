package gopt

import (
	"math"
	"strings"
)

// EstimatedTokenCount calculates an estimation of the max number of tokens for the
// given string.
func EstimatedTokenCount(s string) int {
	etc := int(math.Max(float64(len(s)/4), float64(len(strings.Fields(s))*4)/3) * 1.75)
	return etc
}
