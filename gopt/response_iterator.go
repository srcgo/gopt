package gopt

import (
	"gitlab.com/srcgo/gopt/gopt/collection"
)

type responseIterator[T any] struct {
	resChan chan T
	errChan chan error
	next    func() (T, bool)
}

func newResponseIterator[T any](resChan chan T, errChan chan error) collection.Iterator[T] {
	it := &responseIterator[T]{
		resChan: resChan,
		errChan: errChan,
	}

	it.next = func() (T, bool) {
		next, hasNext := <-it.resChan
		return next, hasNext
	}

	return it
}

func (it *responseIterator[T]) Next() (T, bool) {
	return it.next()
}

func (it *responseIterator[T]) Skip(n int) collection.Iterator[T] {
	for i := 0; i < n; i++ {
		if _, hasNext := it.Next(); !hasNext {
			break
		}
	}

	return it
}

func (it *responseIterator[T]) SkipIf(n int, predicate func(t T) bool) collection.Iterator[T] {
	for i := 0; i < n || n < 0; i++ {
		if next, hasNext := it.Next(); !hasNext {
			break
		} else if !predicate(next) {
			prev := it.next

			it.next = func() (T, bool) {
				it.next = prev
				return next, true
			}

			break
		}
	}

	return it
}

func (it *responseIterator[T]) Error() error {
	if _, ok := <-it.resChan; ok {
		return collection.ErrNotEmpty
	}

	if err, ok := <-it.errChan; ok {
		return err
	}

	return nil
}

func (it *responseIterator[T]) Close() error {
	for range it.resChan {
	}

	return nil
}
