package json

import (
	"encoding/json"
	"strings"
)

// To encodes the object pointed to by p into a json string and returns it.
func To[T any](p *T) (string, error) {
	sb := strings.Builder{}
	enc := json.NewEncoder(&sb)
	err := enc.Encode(p)
	return strings.TrimSpace(sb.String()), err
}
