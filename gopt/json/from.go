package json

import (
	"encoding/json"
	"strings"
)

// From parses the given json string s into an object of type T and returns a
// pointer to the object.
func From[T any](s string) (*T, error) {
	var t T
	p := &t
	dec := json.NewDecoder(strings.NewReader(s))
	return p, dec.Decode(p)
}
