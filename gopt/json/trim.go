package json

import "strings"

// Trim trims the left and right side of the given string from any content that is
// invalid json.
func Trim(s string) string {
	return TrimLeft(TrimRight(s))
}

// TrimLeft trims the left side of the given string from any content that is
// invalid json.
func TrimLeft(s string) string {
	s = strings.TrimSpace(s)

	if len(s) == 0 || s[0] == '{' || s[0] == '[' {
		return s
	}

	objIndx, arrIndx := strings.Index(s, "{"), strings.Index(s, "[")
	var startIndx int

	if objIndx < 0 {
		if arrIndx >= 0 {
			startIndx = arrIndx
		}
	} else if arrIndx < 0 {
		if objIndx >= 0 {
			startIndx = objIndx
		}
	} else if objIndx < arrIndx {
		startIndx = objIndx
	} else if arrIndx < objIndx {
		startIndx = arrIndx
	}

	return s[startIndx:]
}

// TrimRight trims the right side of the given string from any content that is
// invalid json.
func TrimRight(s string) string {
	s = strings.TrimSpace(s)
	l := len(s)

	if l == 0 || s[l-1] == '}' || s[l-1] == ']' {
		return s
	}

	objIndx, arrIndx := strings.LastIndex(s, "}"), strings.LastIndex(s, "]")
	endIndx := l - 1

	if objIndx < 0 {
		if arrIndx >= 0 {
			endIndx = arrIndx
		}
	} else if arrIndx < 0 {
		if objIndx >= 0 {
			endIndx = objIndx
		}
	} else if objIndx > arrIndx {
		endIndx = objIndx
	} else if arrIndx > objIndx {
		endIndx = arrIndx
	}

	return s[:endIndx+1]
}
