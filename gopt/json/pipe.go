package json

// PipeTo interprets all strings received from resChanFrom as json and parses them
// into objects of type T which are sent to the returned result channel. Invalid
// json at the beginning of a string is ignored. The end of the stream of data
// received from resChanFrom is specified by eos (end of stream). Use an empty
// string to read until resChanFrom was closed.
//
// Any errors received from errChanFrom are sent to the returned error channel
// aswell as any errors that may occur while parsing. Both, the returned result and
// error channel, are closed in case of an error - latter only after errChanFrom
// was closed. In either case values will be received from resChanFrom and
// errChanFrom until closed by the sender.
func PipeTo[T any](resChanFrom chan string, errChanFrom chan error, eos string) (chan T, chan error) {
	resChanTo, errChanTo := make(chan T), make(chan error, 2)
	go pipe(resChanFrom, errChanFrom, resChanTo, errChanTo, eos)
	return resChanTo, errChanTo
}

// Helper function to perform the PipeTo operation. The buffer of errChanTo should
// be >= 2.
func pipe[T any](resChanFrom chan string, errChanFrom chan error, resChanTo chan T, errChanTo chan error, eos string) {
	defer redirect(errChanFrom, errChanTo, true)
	defer close(resChanTo)

	var parseErr error
	var t *T

	for res := range resChanFrom {
		if len(eos) > 0 && res == eos {
			go redirect(resChanFrom, nil, false)
			break
		}

		if res = TrimLeft(res); len(res) == 0 {
			continue // separator? caused by "\n\n" from SSE? (todo: make sure this is intended)
		}

		if t, parseErr = From[T](res); parseErr != nil {
			errChanTo <- parseErr // will not block since buffer of errChanTo is (supposed to be) >= 2
			break
		}

		resChanTo <- *t
	}
}

// Redirect receives data from chFrom and sends it to chTo unitl chFrom is closed.
// This function may be used to 'empty' channels by passing nil for chTo.
func redirect[T any](chFrom chan T, chTo chan T, closeTo bool) {
	var do func(t T)

	if chTo == nil {
		do = func(t T) {}
	} else {
		do = func(t T) { chTo <- t }
	}

	for t := range chFrom {
		do(t)
	}

	if closeTo && chTo != nil {
		close(chTo)
	}
}
