package http

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

// Request is a helper function to receive the response body of an http request in
// a channel. Supports SSE (Server Send Events) i.e. the channel stays open until
// the event stream is closed by the server. If an error occurs it is send to the
// error channel and both channels are closed.
func Request(method, url string, header, body map[string]any) (chan string, chan error) {
	resChan, errChan := make(chan string), make(chan error, 1)
	req, err := http.NewRequest(method, url, nil)

	if err == nil {
		setHeader(req, header)

		if err := setBody(req, body); err == nil {
			go perform(req, resChan, errChan) // closes the channels
		} else {
			errChan <- err // will not block since errChan has a buffer of 1
			close(resChan)
			close(errChan)
		}
	} else {
		errChan <- err // will not block since errChan has a buffer of 1
		close(resChan)
		close(errChan)
	}

	return resChan, errChan
}

// Performs a http request and sends the results to the given channels.
func perform(req *http.Request, resChan chan string, errChan chan error) {
	defer close(errChan)
	defer close(resChan)

	if resp, err := http.DefaultClient.Do(req); err == nil {
		var readErr error
		var text []byte

		if resp.Header.Get("Content-Type") == "text/event-stream" {
			// DEP protocol
			rd := bufio.NewReader(resp.Body)

			for readErr == nil {
				// discard leading 2 newline characters from the response
				if _, readErr = rd.Discard(2); readErr == nil {
					if text, readErr = rd.ReadBytes('\n'); readErr == nil {
						stext := string(text)
						resChan <- strings.TrimSpace(stext)

						if strings.HasSuffix(stext, "\n") {
							// unread newline character which is part of the prefix from the next response
							rd.UnreadByte()
						}
					}
				}
			}
		} else if text, readErr := io.ReadAll(resp.Body); readErr == nil {
			resChan <- strings.TrimSpace(string(text))
		}

		if readErr != nil && readErr != io.EOF {
			errChan <- readErr // will not block since errChan has a buffer of 1
		}
	} else {
		errChan <- err // will not block since errChan has a buffer of 1
	}
}

func setHeader(req *http.Request, header map[string]any) {
	if header != nil {
		for k, v := range header {
			req.Header.Set(k, fmt.Sprintf("%v", v))
		}
	}
}

func setBody(req *http.Request, body map[string]any) error {
	if body != nil {
		sb := strings.Builder{}

		if err := json.NewEncoder(&sb).Encode(body); err != nil {
			return err
		}

		req.Body = io.NopCloser(strings.NewReader(sb.String()))
	}

	return nil
}
