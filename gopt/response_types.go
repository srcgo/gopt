package gopt

import "gitlab.com/srcgo/gopt/gopt/chat"

type openAIResponse struct {
	Error *struct{ Message string }
}

type ModelsResponse struct {
	openAIResponse
	Data []struct {
		Id      string
		OwnedBy string
		// Permissions []string // ?
	}
}

type ModelResponse struct {
	openAIResponse
	Id      string
	OwnedBy string
	// Permissions []string // ?
}

type CompletionResponse struct {
	openAIResponse
	Id      string
	Created int
	Model   string
	Choices []struct {
		Text         string
		FinishReason string
		Index        int
	}
	Usage struct {
		PromptTokens     int
		CompletionTokens int
		TotalTokens      int
	}
}

type ChatCompletionResponse struct {
	openAIResponse
	Id      string
	Object  string
	Created int
	Model   string
	Usage   struct {
		PromptTokens     int
		CompletionTokens int
		TotalTokens      int
	}
	Choices []struct {
		Message struct {
			Role    chat.Role
			Content string
		}
		Delta struct {
			Role    *chat.Role
			Content *string
		}
		FinishReason string
		Index        int
	}
}

type EditResponse struct {
	openAIResponse
	Created int
	Choices []struct {
		Text         string
		FinishReason string
		Index        int
	}
	Usage struct {
		PromptTokens     int
		CompletionTokens int
		TotalTokens      int
	}
}

type ImageResponse struct {
	openAIResponse
	Created int
	Data    []struct {
		Url     string
		B64Json string
	}
}

type ImageEditResponse struct {
	openAIResponse
	ImageResponse
}

type ImageVariationResponse struct {
	openAIResponse
	ImageResponse
}

type EmbeddingResponse struct {
	openAIResponse
	Data []struct {
		Embedding EmbeddingVector
	}
	Model string
	Usage struct {
		PromptTokens int
		TotalTokens  int
	}
}
