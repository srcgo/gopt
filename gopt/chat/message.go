package chat

import (
	"encoding/json"

	gopt_json "gitlab.com/srcgo/gopt/gopt/json"
)

type Message struct {
	json.Marshaler
	Role    Role
	Content string
}

func (m Message) MarshalJSON() ([]byte, error) {
	t := map[string]string{
		"role":    string(m.Role),
		"content": m.Content,
	}

	s, err := gopt_json.To(&t)
	return []byte(s), err
}
