package chat

type Role string

const (
	System    Role = "system"
	Assistant Role = "assistant"
	User      Role = "user"
)
