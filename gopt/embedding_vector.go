package gopt

import "math"

type EmbeddingVector []float64

func (v EmbeddingVector) CosineSimilarity(u EmbeddingVector) float64 {
	if p := v.Length() * u.Length(); p != 0 {
		return v.DotProduct(u) / p
	}

	return 0
}

func (v EmbeddingVector) DotProduct(u EmbeddingVector) float64 {
	if len(v) != len(u) {
		panic("invalid arguments, vectors must be of the same dimension")
	}

	var a float64

	for i, x := range v {
		a += x * u[i]
	}

	return a
}

func (v EmbeddingVector) Length() float64 {
	var a float64

	for _, x := range v {
		a += x * x
	}

	return math.Sqrt(a)
}
