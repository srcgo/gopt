package collection

import (
	"errors"
	"io"
)

var ErrNotEmpty = errors.New("collection: invalid operation, source not empty")

// Iterator is an interface that declares the functionality to iterate over a set
// of elements. Note that an Iterator only needs to be closed if it was not used to
// iterate until Next indicated that no more elements were available. Close always
// returns a nil error.
type Iterator[T any] interface {
	io.Closer

	// Next returns the next value of the iteration. The second return value indicates
	// wether there was another value to retrieve.
	Next() (T, bool)

	// Skip skips the next specified amount of elements.
	Skip(int) Iterator[T]

	// SkipIf will skip up to n values as long as each value meets the condition
	// specified by the given function. If a negative n is used, SkipIf will stop when
	// the condition is not met or when the iteration is completed.
	SkipIf(int, func(T) bool) Iterator[T]

	// Error returns any error that might have occurred during iteration. May only be
	// called if there were no more elements to retrieve or ErrNotEmpty is returned.
	Error() error
}
