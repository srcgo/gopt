package gopt

import (
	"os"

	"gitlab.com/srcgo/gopt/gopt/json"
)

// Config defines the configuration type for clients.
type Config struct {
	Models         requestConfig
	Model          requestConfig
	Completion     requestConfig
	ChatCompletion requestConfig
	Edit           requestConfig
	Image          requestConfig
	ImageEdit      requestConfig
	ImageVariation requestConfig
	Embedding      requestConfig
}

type requestConfig struct {
	Header map[string]any // http-request header
	Body   map[string]any // http-request body
	EOS    string         // end of stream (for SSE)
}

func (rc *requestConfig) clone() requestConfig {
	header := map[string]any{}
	body := map[string]any{}

	for k, v := range rc.Header {
		header[k] = v
	}

	for k, v := range rc.Body {
		body[k] = v
	}

	return requestConfig{
		Header: header,
		Body:   body,
		EOS:    rc.EOS,
	}
}

func (rc *requestConfig) HeaderWith(kv map[string]any) map[string]any {
	header := map[string]any{}

	for k, v := range rc.Header {
		header[k] = v
	}

	for k, v := range kv {
		header[k] = v
	}

	return header
}

func (rc *requestConfig) BodyWith(kv map[string]any) map[string]any {
	body := map[string]any{}

	for k, v := range rc.Body {
		body[k] = v
	}

	for k, v := range kv {
		body[k] = v
	}

	return body
}

// Name of the environment variable pointing to the config file.
const ConfigFileEnvVar = "GOPT_CONFIG_FILE"

// DefaultConfig will be instantiated with one of the following ways:
// - parsed from gopt-config.json in the current working directory (todo)
// - parsed from the file located at GOPT_CONFIG_FILE
// - created with handpicked default parameters
var DefaultConfig Config

func init() {
	if path := os.Getenv(ConfigFileEnvVar); len(path) > 0 {
		if p, err := json.From[Config](path); err != nil {
			panic(err)
		} else {
			DefaultConfig = *p
		}
	} else {
		DefaultConfig = Config{
			Completion: requestConfig{
				Body: map[string]any{
					"model": "text-davinci-003",
				},
				EOS: "data: [DONE]",
			},
			ChatCompletion: requestConfig{
				Body: map[string]any{
					"model": "gpt-3.5-turbo",
				},
				EOS: "data: [DONE]",
			},
			Edit:           requestConfig{},
			Image:          requestConfig{},
			ImageEdit:      requestConfig{},
			ImageVariation: requestConfig{},
			Embedding: requestConfig{
				Body: map[string]any{
					"model": "text-embedding-ada-002",
				},
			},
		}
	}
}

// Creates a new config based of DefaultConfig.
func NewConfig() Config {
	return Config{
		Completion:     DefaultConfig.Completion.clone(),
		ChatCompletion: DefaultConfig.ChatCompletion.clone(),
		Edit:           DefaultConfig.Edit.clone(),
		Image:          DefaultConfig.Image.clone(),
		ImageEdit:      DefaultConfig.ImageEdit.clone(),
		ImageVariation: DefaultConfig.ImageVariation.clone(),
		Embedding:      DefaultConfig.Embedding.clone(),
	}
}
