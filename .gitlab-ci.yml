stages:
  - init
  - test
  - build
  - upload
  - release

variables:
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"
  SEMVER_REGEX: ^v[0-9]+\.[0-9]+\.[0-9]+$

before_script:
  # Package version can only contain numbers (0-9), and dots (.).
  # Must be in the format of X.Y.Z, i.e. should match /\A\d+\.\d+\.\d+\z/ regular expresion.
  # See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
  - export PACKAGE_VERSION=$(echo "$CI_COMMIT_TAG" | cut -b2-)

test:
  stage: test
  image: golang:1.19
  only:
    # - /$SEMVER_REGEX/ how?
    - /v[0-9]+\.[0-9]+\.[0-9]+/
    - release
    - master
    - main
  except:
    - triggers
  script:
    - apt-get update
    - apt install libx11-dev -y
    - go test ./... -count 1

build:
  stage: build
  image: golang:1.19
  only:
    # - /$SEMVER_REGEX/ how?
    - /v[0-9]+\.[0-9]+\.[0-9]+/
  except:
    # tags only
    - branches
    - triggers
  script:
    - apt-get update
    - apt install libx11-dev -y
    - git tag -l --format='%(contents)' $CI_COMMIT_TAG > tag-desc
    - ./godist > dist-paths
  artifacts:
    paths:
      - dist-paths
      - tag-desc
      - dist/

upload:
  stage: upload
  image: bitnami/git:latest
  only:
    # - /$SEMVER_REGEX/ how?
    - /v[0-9]+\.[0-9]+\.[0-9]+/
  except:
    # tags only
    - branches
    - triggers
  script:
    - |+
      while read -r path; do
          arch=$(dirname $path)
          os=$(basename $(dirname $arch))
          arch=$(basename $arch)
          exe=$(basename $path)
          pkg=${exe%%.*}-$os-$arch
          curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file dist/$os/$arch/$exe ${PACKAGE_REGISTRY_URL}/$pkg/$PACKAGE_VERSION/$exe
          echo --assets-link "{\"name\":\"$pkg\",\"url\":\"${PACKAGE_REGISTRY_URL}/$pkg/$PACKAGE_VERSION/$exe\"}" >> asset-links
      done < dist-paths
  artifacts:
    paths:
      # todo: kinda insecure :/
      - asset-links

release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  only:
    # - /$SEMVER_REGEX/ how?
    - /v[0-9]+\.[0-9]+\.[0-9]+/
  except:
    # tags only
    - branches
    - triggers
  script:
    - |+
      while read -r asset; do
        assets="$assets $asset"
      done < asset-links
    - desc=$(cat tag-desc)
    - release-cli create --name "Release $CI_COMMIT_TAG" --description "$desc" --tag-name $CI_COMMIT_TAG $assets
