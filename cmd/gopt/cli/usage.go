package cli

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/srcgo/gopt/gopt"
)

func usage() {
	fmt.Fprintln(os.Stderr, `Description:
  Interact with the OpenAI API (https://openai.com/api/) from the command line. An
  API key needs to be provided with the environment variable `+gopt.ApiKeyEnvVar+`. An
  optional organization id may be set with `+gopt.OrganizationIdEnvVar+`. Input can be
  provided by piping it into the program and/or as additional arguments. Any piped
  input will be appended to input provided as arguments, separated by a newline
  character. When run as interactive chat, the initial input is sent immediately
  without being displayed in the chatlog.`)

	fmt.Fprintln(os.Stderr, "\nUsage:")
	flag.PrintDefaults()
}
