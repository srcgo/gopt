package cli

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/srcgo/gopt/cmd/gopt/gui"
	"gitlab.com/srcgo/gopt/gopt"
)

var modeActions = map[string]func(gopt.Client, string, io.Writer) error{
	"completion":      func(c gopt.Client, s string, w io.Writer) error { return c.WriteCompletion(s, w) },
	"edit":            func(c gopt.Client, s string, w io.Writer) error { return c.WriteEdit(s, w) },
	"image":           func(c gopt.Client, s string, w io.Writer) error { return c.WriteImage(s, w) },
	"image-edit":      func(c gopt.Client, s string, w io.Writer) error { return c.WriteImageEdit(s, w) },
	"image-variation": func(c gopt.Client, s string, w io.Writer) error { return c.WriteImageVariation(s, w) },
	// "embedding":       embedding, // todo
}

func Perform(client gopt.Client) error {
	if fn, has := modeActions[*modeFlag]; has {
		if *chatFlag {
			app := gui.NewTermApp(client)

			if len(inputFlag) > 0 {
				if _, err := app.Write([]byte(inputFlag)); err != nil {
					return err
				}
			}

			return app.Run()
		} else {
			if len(inputFlag) > 0 {
				return fn(client, inputFlag, os.Stdout)
			}
		}
	} else {
		return fmt.Errorf("no such action '%s'", *modeFlag)
	}

	return nil
}
