package cli

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var (
	modeFlag  = flag.String("mode", "completion", "Mode of operation (completion|edit|image|image-edit|image-variation|\nembedding)")
	chatFlag  = flag.Bool("chat", false, "Run as interactive chat (implies completion mode)")
	inputFlag = ""
)

func init() {
	os.Args[0] = filepath.Base(os.Args[0])
	flag.Usage = usage
	flag.Parse()

	if *chatFlag {
		*modeFlag = "completion"
	}

	// https://stackoverflow.com/a/26567513
	stat, _ := os.Stdin.Stat()
	var piped []byte

	if (stat.Mode() & os.ModeCharDevice) == 0 {
		var err error

		if piped, err = ioutil.ReadAll(os.Stdin); err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			os.Exit(1)
		}
	}

	args := flag.Args()

	if len(piped) > 0 {
		if len(args) > 0 {
			inputFlag = strings.Join(args, " ") + "\n\n" + string(piped)
		} else {
			inputFlag = string(piped)
		}
	} else if len(args) > 0 {
		inputFlag = strings.Join(args, " ")
	}
}
