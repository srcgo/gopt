package chat

import (
	"container/list"
	"sort"
	"time"

	"gitlab.com/srcgo/gopt/gopt"
)

// Prompt provides an interface for interacting with messages in a chat history.
type Prompt interface {
	// StartMessage creates and starts a new message.
	StartMessage(string, string) Message

	// FinishMessage finishes the given Message. Further Write calls with that Message
	// will trigger a panic.
	FinishMessage(Message) Message

	// StoreMessage stores the given Message in the database of the prompt. Note that
	// only finished Messages should be stored.
	StoreMessage(Message) Message

	// Matches retrieves the most matching messages from the database until the total
	// token count of all retrieved messages reaches the given max token count. The
	// returned Messages are ordered ascending by the dates they have been sent. Note
	// that the given Message should be finished and may not be stored in the database
	// (otherwise it will be included in the result).
	Matches(Message, int) []Message
}

type prompt struct {
	client       gopt.Client
	db           Database
	messageCount int
}

// NewPrompt creates a new prompt instance.
func NewPrompt(client gopt.Client) Prompt {
	return &prompt{client: client, db: NewInMemoryDatabase()}
}

func (p *prompt) StartMessage(sender, color string) Message {
	m := message{
		id:            p.messageCount,
		sender:        sender,
		color:         color,
		embeddingChan: make(chan gopt.EmbeddingVector, 1),
	}

	p.messageCount++
	return &m
}

func (p *prompt) FinishMessage(m Message) Message {
	if mp, ok := m.(*message); ok {
		go p.fetchEmbedding(mp)
	}

	if err := m.Close(); err != nil {
		panic(err) // todo
	}

	return m
}

func (p *prompt) StoreMessage(m Message) Message {
	p.db.Store(m)
	return m
}

func (p *prompt) Matches(m Message, maxTokenCount int) []Message {
	if maxTokenCount <= 0 {
		return []Message{}
	}

	type MessageSimilarity struct {
		message    Message
		similarity float64
		tokens     int
	}

	totalTokenCount := 0
	best := list.New()

	p.db.Select(func(n Message) bool {
		s := m.Embedding().CosineSimilarity(n.Embedding())
		tc := gopt.EstimatedTokenCount(n.Content()) + 16
		var added bool

		for next := best.Front(); next != nil; next = next.Next() {
			if s < next.Value.(MessageSimilarity).similarity {
				best.InsertBefore(MessageSimilarity{n, s, tc}, next)
				added = true
				break
			}
		}

		if !added {
			best.PushBack(MessageSimilarity{n, s, tc})
		}

		totalTokenCount += tc

		for totalTokenCount > maxTokenCount {
			ms := best.Remove(best.Front()).(MessageSimilarity)
			totalTokenCount -= ms.tokens
		}

		return false // we store our results in best
	}, -1)

	messages := make([]Message, best.Len())

	for i, next := 0, best.Front(); next != nil; i, next = i+1, next.Next() {
		messages[i] = next.Value.(MessageSimilarity).message
	}

	sort.Slice(messages, func(i, j int) bool {
		return messages[i].Timestamp().Before(messages[j].Timestamp())
	})

	return messages
}

// Fetches the embedding vector for the given message and sends it to the
// embeddingChan.
func (p *prompt) fetchEmbedding(m *message) {
	it := p.client.CreateEmbedding(m.Content())

	if next, hasNext := it.Next(); hasNext {
		if next.Error != nil {
			time.Sleep(time.Second) // todo: max retries (and somehow catch and report error)
			defer p.fetchEmbedding(m)
			return
		}

		m.embeddingChan <- gopt.EmbeddingVector(next.Data[0].Embedding)
	}

	if err := it.Error(); err != nil {
		panic(err.Error()) // todo
	}
}
