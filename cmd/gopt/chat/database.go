package chat

type Database interface {
	// Store stores the given Message in the database. If a Message with the same
	// content already exists it will be overwritten by the given Message.
	Store(Message)

	// Select retrieves up to the given amount of Messages from the database that
	// returned true for the given predicate. Note that this method will always
	// traverse and check all Messages unless the specified limit is 0. This means that
	// with a predicate returnig true for two Messages and a limit of 1 the latter
	// Message will not be included in the result (even though the predicate function
	// is executed for both Messages). A negative limit means there is no limit.
	Select(func(Message) bool, int) []Message
}

// In-Memory database.
type database struct {
	messages map[string]Message
}

// Creates a new in-memory database.
func NewInMemoryDatabase() Database {
	return &database{messages: map[string]Message{}}
}

func (db *database) Store(m Message) {
	db.messages[m.Content()] = m
}

func (db *database) Select(predicate func(Message) bool, limit int) []Message {
	var messages []Message

	if limit == 0 {
		return messages
	}

	if limit < 0 {
		messages = make([]Message, 0, len(db.messages))
	} else {
		messages = make([]Message, 0, limit)
	}

	messages_cap := cap(messages)

	for _, message := range db.messages {
		if predicate(message) && len(messages) < messages_cap {
			messages = append(messages, message)
		}
	}

	return messages
}
