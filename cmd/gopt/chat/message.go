package chat

import (
	"fmt"
	"io"
	"time"

	"github.com/rivo/tview"
	"gitlab.com/srcgo/gopt/gopt"
)

var TimeFormat = "2006-02-01 15:04:05" // ISO 8601

type Message interface {
	io.WriteCloser
	io.WriterTo
	Sender() string
	Content() string
	// Prefix() string
	Embedding() gopt.EmbeddingVector
	Timestamp() time.Time
}

type message struct {
	id            int
	sender        string
	color         string
	buffer        []byte
	timestamp     time.Time
	embedding     *gopt.EmbeddingVector
	embeddingChan chan gopt.EmbeddingVector
	targets       []io.Writer
	closed        bool
	wasWrittenTo  bool
}

func (m *message) Sender() string {
	return m.sender
}

func (m *message) Content() string {
	return string(m.buffer)
}

func (m *message) Prefix() string {
	nl := ""

	if m.id != 0 {
		nl = "\n"
	}

	return fmt.Sprintf(
		"%s[%s]%s %s>[white]\n[\"%d\"]",
		nl, m.color, m.timestamp.Format(TimeFormat), m.sender, m.id)
}

func (m *message) Timestamp() time.Time {
	return m.timestamp
}

func (m *message) Embedding() gopt.EmbeddingVector {
	if m.embedding == nil {
		e := <-m.embeddingChan
		m.embedding = &e
	}

	return *m.embedding
}

func (m *message) Write(p []byte) (int, error) {
	if m.closed {
		panic("Write attempt to closed message")
	}

	if !m.wasWrittenTo {
		m.timestamp = time.Now()
		m.wasWrittenTo = true

		for _, w := range m.targets {
			if n, err := m.writeBuffer(w); err != nil {
				return n, err
			}
		}
	}

	m.buffer = append(m.buffer, p...)
	p = []byte(tview.Escape(string(p)))

	for _, w := range m.targets {
		if n, err := w.Write(p); err != nil {
			return n, err
		}
	}

	return len(p), nil
}

func (m *message) WriteTo(w io.Writer) (int64, error) {
	var n1, n2 int
	var err error

	if m.wasWrittenTo {
		if n1, err = m.writeBuffer(w); err != nil {
			return int64(n1), err
		}
	}

	if m.closed {
		if n2, err = w.Write([]byte("[\"\"]\n")); err != nil {
			return int64(n2), err
		}
	} else {
		m.targets = append(m.targets, w)
	}

	return int64(n1 + n2), err
}

func (m *message) writeBuffer(w io.Writer) (int, error) {
	var n1, n2 int
	var err error

	if n1, err = w.Write([]byte(m.Prefix())); err != nil {
		return n1, err
	}

	if n2, err = w.Write([]byte(tview.Escape(string(m.buffer)))); err != nil {
		return n2, err
	}

	return (n1 + n2), err
}

// This method may only be called once for a Message. Further write operations
// won't be possible anymore.
func (m *message) Close() error {
	if m.closed {
		panic("Attempt to close an already closed message")
	}

	if !m.wasWrittenTo {
		m.timestamp = time.Now()
	}

	for _, w := range m.targets {
		if !m.wasWrittenTo {
			if _, err := w.Write([]byte(m.Prefix())); err != nil {
				return err
			}
		}

		if _, err := w.Write([]byte("[\"\"]\n")); err != nil {
			return err
		}
	}

	m.closed = true
	m.targets = nil
	return nil
}
