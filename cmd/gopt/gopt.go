package main

import (
	"fmt"
	"os"

	"gitlab.com/srcgo/gopt/cmd/gopt/cli"
	"gitlab.com/srcgo/gopt/gopt"
)

func main() {
	config := gopt.NewConfig()
	config.Completion.Body["stream"] = true
	config.Completion.Body["temperature"] = 0.5
	config.Completion.Body["frequency_penalty"] = 0.5
	config.ChatCompletion.Body["stream"] = true
	config.ChatCompletion.Body["temperature"] = 0.5
	config.ChatCompletion.Body["frequency_penalty"] = 0.5

	client := gopt.NewClientConfigured(
		os.Getenv(gopt.ApiKeyEnvVar),
		os.Getenv(gopt.OrganizationIdEnvVar),
		config)

	if err := cli.Perform(client); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
