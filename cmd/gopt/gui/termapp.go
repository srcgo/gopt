package gui

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"gitlab.com/srcgo/gopt/cmd/gopt/chat"
	"gitlab.com/srcgo/gopt/gopt"
	"golang.design/x/clipboard"
)

type TermApp struct {
	// app layout
	app     *tview.Application
	root    *tview.Flex
	inHead  *tview.TextView
	in      *tview.TextArea
	outHead *tview.TextView
	out     *tview.TextView
	info    *tview.TextView

	// app properties
	client       gopt.Client
	locked       bool
	zoomed       bool
	userPrompt   bool
	selected     int
	messageCount int
	clipboard    string
	prompt       chat.Prompt
	buffer       *strings.Builder
}

const GOPT_VERSION = "v0.2.0"
const TIME_FORMAT = "2006-02-01 15:04:05" // ISO 8601

const apiTokenLimit = 4096
const promptTokenLimit = 256

var clipboardSupport bool

func init() {
	clipboardSupport = clipboard.Init() == nil
}

func NewTermApp(client gopt.Client) *TermApp {
	tapp := TermApp{
		client: client,
		buffer: &strings.Builder{},
		prompt: chat.NewPrompt(client),
	}

	tapp.initInHead()
	tapp.initOutHead()
	tapp.initIn()
	tapp.initOut()
	tapp.initInfo()
	tapp.initRoot()
	tapp.initApp()
	tapp.showMessageView(false)
	return &tapp
}

func (tapp *TermApp) Write(p []byte) (n int, err error) {
	if !tapp.locked {
		tapp.request(string(p))
		return len(p), nil
	} else {
		return 0, fmt.Errorf("application is locked")
	}
}

func (tapp *TermApp) Run() error {
	return tapp.app.Run()
}

func (tapp *TermApp) initApp() {
	tapp.app = tview.NewApplication()
	tapp.app.SetRoot(tapp.root, true)
	tapp.app.SetInputCapture(tapp.handleInputApp)
}

func (tapp *TermApp) initRoot() {
	tapp.root = tview.NewFlex()
	tapp.root.SetDirection(tview.FlexColumnCSS)
	tapp.root.AddItem(tapp.outHead, 1, 0, false)
	tapp.root.AddItem(tapp.out, 0, 3, false)
	tapp.root.AddItem(tapp.inHead, 1, 0, false)
	tapp.root.AddItem(tapp.in, 0, 1, true)
	tapp.root.AddItem(tapp.info, 1, 0, false)
}

func (tapp *TermApp) initInHead() {
	tapp.inHead = tview.NewTextView().SetText("message")
	tapp.inHead.SetBackgroundColor(tcell.Color104)
}

func (tapp *TermApp) initIn() {
	tapp.in = tview.NewTextArea()
	tapp.in.SetWordWrap(true)
	tapp.in.SetClipboard(tapp.toClipboard, tapp.fromClipboard)
	tapp.in.SetInputCapture(tapp.handleInputIn)
}

func (tapp *TermApp) initOutHead() {
	tapp.outHead = tview.NewTextView().SetText(fmt.Sprintf("gopt %s", GOPT_VERSION))
	tapp.outHead.SetBackgroundColor(tcell.Color104)
}

func (tapp *TermApp) initOut() {
	tapp.out = tview.NewTextView()
	tapp.out.SetDynamicColors(true)
	tapp.out.SetRegions(true)
	tapp.out.SetWordWrap(true)
	tapp.out.SetBlurFunc(func() { tapp.out.Highlight() })
	tapp.out.SetFocusFunc(func() { tapp.selectMessage(tapp.selected) })
	tapp.out.SetInputCapture(tapp.handleInputOut)
}

func (tapp *TermApp) initInfo() {
	tapp.info = tview.NewTextView()
	tapp.info.SetDynamicColors(true)
	tapp.info.SetWordWrap(true)
}

func (tapp *TermApp) setInfo(pairs ...[2]string) {
	sb := strings.Builder{}

	for _, pair := range pairs {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}

		sb.WriteString(fmt.Sprintf("[green]%s:[white] %s", pair[0], pair[1]))
	}

	tapp.info.SetText(sb.String())
}

func (tapp *TermApp) showMessageView(zoomed bool) {
	if zoomed {
		tapp.root.ResizeItem(tapp.out, 0, 0)
		tapp.root.ResizeItem(tapp.in, 0, 1)
		tapp.zoomed = true
		tapp.setInfo(
			[2]string{"M-M", "Split View"},
			[2]string{"M-K", "Chat View"},
			[2]string{"M-S", "Send"},
			[2]string{"Esc", "Quit"},
		)
	} else {
		tapp.root.ResizeItem(tapp.out, 0, 3)
		tapp.root.ResizeItem(tapp.in, 0, 1)
		tapp.zoomed = false
		tapp.setInfo(
			[2]string{"M-M", "Message View"},
			[2]string{"M-K", "Chat View"},
			[2]string{"M-S", "Send"},
			[2]string{"Esc", "Quit"},
		)
	}

	if !tapp.in.HasFocus() {
		tapp.app.SetFocus(tapp.in)
	}
}

func (tapp *TermApp) showChatView() {
	if !tapp.out.HasFocus() {
		tapp.root.ResizeItem(tapp.out, 0, 1)
		tapp.root.ResizeItem(tapp.in, 0, 0)
		tapp.app.SetFocus(tapp.out)
		tapp.setInfo(
			[2]string{"M-M", "Message View"},
			[2]string{"M-K", "Split View"},
			[2]string{"PgUp", "Previous"},
			[2]string{"PgDn", "Next"},
			[2]string{"Back", "Deselect"},
			[2]string{"Esc", "Quit"},
		)
	}
}

func (tapp *TermApp) handleInputApp(event *tcell.EventKey) *tcell.EventKey {
	var result *tcell.EventKey

	switch event.Key() {
	case tcell.KeyESC:
		tapp.app.Stop()
	case tcell.KeyCtrlL: // remapped to Ctrl+A (select all)
	case tcell.KeyCtrlA:
		result = tcell.NewEventKey(tcell.KeyCtrlL, 'L', tcell.ModCtrl)
	case tcell.KeyCtrlQ: // remapped to Ctrl+C (copy to clipboard)
	case tcell.KeyCtrlC:
		result = tcell.NewEventKey(tcell.KeyCtrlQ, 'Q', tcell.ModCtrl)
	case tcell.KeyCtrlH: // disabled (Delete one character to the left of the cursor) -> use Backspace
		if event.Modifiers() == tcell.ModNone {
			result = event // only tcell.KeyBackspace without modifiers
		}
	case tcell.KeyCtrlD: // disabled (Delete the character under the cursor (or the first) -> use Delete
	case tcell.KeyCtrlE: // disabled (Move to the end of the current line) -> use Home
	case tcell.KeyCtrlF: // disabled (Move down by one page)
	case tcell.KeyCtrlB: // disabled (Move up by one page)
	case tcell.KeyCtrlK: // disabled (Delete everything under and to the right of the cursor until the next newline character)
	// case tcell.KeyCtrlU: // disabled (Delete current line) -> use Ctrl+X without selection (currently not implemented)
	default:
		result = event
	}

	return result
}

func (tapp *TermApp) handleInputIn(event *tcell.EventKey) *tcell.EventKey {
	var result *tcell.EventKey

	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 's':
			if event.Modifiers()&tcell.ModAlt == tcell.ModAlt {
				if !tapp.locked {
					if text := tapp.in.GetText(); len(strings.Trim(text, " \t\r\n")) > 0 {
						tapp.app.QueueEvent(tcell.NewEventKey(tcell.KeyCtrlA, 'A', tcell.ModCtrl))
						tapp.app.QueueEvent(tcell.NewEventKey(tcell.KeyBackspace, rune(0), tcell.ModNone))
						tapp.respond(tapp.request(text))
					}
				}
			} else {
				result = event
			}
		case 'm':
			if event.Modifiers()&tcell.ModAlt == tcell.ModAlt {
				tapp.showMessageView(!tapp.zoomed)
			} else {
				result = event
			}
		case 'k':
			if event.Modifiers()&tcell.ModAlt == tcell.ModAlt {
				tapp.showChatView()
			} else {
				result = event
			}
		default:
			// we need to strip the modifier of some runes for them to be printed (possible bug in tview)
			result = tcell.NewEventKey(event.Key(), event.Rune(), tcell.ModNone)
		}
	default:
		result = event
	}

	return result
}

func (tapp *TermApp) handleInputOut(event *tcell.EventKey) *tcell.EventKey {
	var result *tcell.EventKey

	switch event.Key() {
	case tcell.KeyPgUp:
		tapp.selectMessage(tapp.selected - 1)
	case tcell.KeyPgDn:
		tapp.selectMessage(tapp.selected + 1)
	case tcell.KeyHome:
		tapp.selectMessage(0)
	case tcell.KeyEnd:
		tapp.selectMessage(tapp.messageCount - 1)
		tapp.out.ScrollToEnd()
	case tcell.KeyBackspace:
		tapp.out.Highlight()
	case tcell.KeyCtrlQ:
		if tapp.selected >= 0 && tapp.selected < tapp.messageCount {
			tapp.toClipboard(tapp.out.GetRegionText(fmt.Sprintf("%d", tapp.selected)))
		}
	case tcell.KeyRune:
		switch event.Rune() {
		case 'm':
			if event.Modifiers()&tcell.ModAlt == tcell.ModAlt {
				tapp.showMessageView(true)
			} else {
				result = event
			}
		case 'k':
			if event.Modifiers()&tcell.ModAlt == tcell.ModAlt {
				tapp.showMessageView(false)
			} else {
				result = event
			}
		default:
			result = event
		}
	default:
		result = event
	}

	return result
}

func (tapp *TermApp) selectMessage(index int) {
	if index >= 0 && index < tapp.messageCount {
		if !tapp.locked {
			tapp.out.Highlight(fmt.Sprintf("%d", index)).ScrollToHighlight()
		}

		tapp.selected = index
	}
}

func (tapp *TermApp) request(text string) chat.Message {
	m := tapp.prompt.StartMessage("you", "red")
	m.WriteTo(tapp.out)
	m.Write([]byte(text))
	tapp.prompt.FinishMessage(m)
	tapp.messageCount++
	tapp.out.ScrollToEnd()
	return m
}

func (tapp *TermApp) respond(m chat.Message) {
	tapp.locked = true

	go func() {
		maxTokenCount := min(promptTokenLimit, apiTokenLimit-(gopt.EstimatedTokenCount(m.Content())+16)-gopt.EstimatedTokenCount(task))
		prompt := prepareChatPrompt(append(tapp.prompt.Matches(m, maxTokenCount), m))
		r := tapp.prompt.StartMessage("gpt", "blue")
		r.WriteTo(updateWriter{tapp})

		if err := tapp.client.WriteChatCompletion(prompt, r); err == nil {
			tapp.prompt.FinishMessage(r)
			tapp.prompt.StoreMessage(m)
			tapp.prompt.StoreMessage(r)
		} else {
			s := tapp.prompt.StartMessage("system", "yellow")
			s.WriteTo(updateWriter{tapp})
			s.Write([]byte(err.Error()))
			tapp.prompt.FinishMessage(s)
		}

		tapp.selected = tapp.messageCount
		tapp.messageCount++

		tapp.app.QueueUpdateDraw(func() {
			tapp.out.ScrollToEnd()
			tapp.locked = false
		})
	}()
}

func (tapp *TermApp) toClipboard(text string) {
	// internal clipboard buffer
	tapp.clipboard = text

	// clipboard of the operating system
	if clipboardSupport {
		clipboard.Write(clipboard.FmtText, []byte(text))
	}
}

func (tapp *TermApp) fromClipboard() string {
	if clipboardSupport {
		return string(clipboard.Read(clipboard.FmtText))
	}

	return tapp.clipboard
}

type updateWriter struct {
	tapp *TermApp
}

func (ur updateWriter) Write(p []byte) (int, error) {
	ur.tapp.app.QueueUpdateDraw(func() {
		ur.tapp.out.Write(p)
	})

	return 0, nil
}
