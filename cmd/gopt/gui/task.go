package gui

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/srcgo/gopt/cmd/gopt/chat"
	gopt_chat "gitlab.com/srcgo/gopt/gopt/chat"
)

var task = `You are a Chatbot named "%s". Provide an answer to the last message from "%s".`

func init() {
	botName := "Chad"
	userName := os.Getenv("USERNAME")

	if len(userName) == 0 {
		userName = "an anonymous person"
	}

	task = fmt.Sprintf(task, botName, userName)
}

// deprecated (use prepareChatPrompt instead)
func preparePrompt(messages []chat.Message) string {
	sb := strings.Builder{}
	sb.WriteString(task)

	for _, m := range messages {
		sb.WriteString(fmt.Sprintf("%s <<(%s)>>\n%s\n", m.Timestamp().Format(chat.TimeFormat), m.Sender(), m.Content()))
	}

	sb.WriteString(fmt.Sprintf("%s <<(gpt)>>\n", time.Now().Format(chat.TimeFormat)))
	return sb.String()
}

func prepareChatPrompt(messages []chat.Message) []gopt_chat.Message {
	gopt_messages := []gopt_chat.Message{gopt_chat.Message{Role: gopt_chat.System, Content: task}}
	var role gopt_chat.Role

	for _, m := range messages {
		switch m.Sender() {
		case "you":
			role = gopt_chat.User
		case "gpt":
			role = gopt_chat.Assistant
		default:
			role = gopt_chat.System
		}

		gopt_messages = append(gopt_messages, gopt_chat.Message{Role: role, Content: m.Content()})
	}

	return gopt_messages
}
