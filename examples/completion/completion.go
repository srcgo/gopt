package main

import (
	"fmt"
	"os"

	"gitlab.com/srcgo/gopt/gopt"
)

func main() {
	client := gopt.NewClient(
		os.Getenv(gopt.ApiKeyEnvVar),
		os.Getenv(gopt.OrganizationIdEnvVar))

	request := "Give me two paragraphs."
	err := client.WriteCompletion(request, os.Stdout)

	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
