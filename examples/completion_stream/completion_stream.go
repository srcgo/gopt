package main

import (
	"fmt"
	"os"

	"gitlab.com/srcgo/gopt/gopt"
)

func main() {
	config := gopt.NewConfig()
	config.Completion.Body["stream"] = true

	client := gopt.NewClientConfigured(
		os.Getenv(gopt.ApiKeyEnvVar),
		os.Getenv(gopt.OrganizationIdEnvVar),
		config)

	request := "Give me two paragraphs."
	err := client.WriteCompletion(request, os.Stdout)

	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
