package main

import (
	"fmt"
	"os"

	"gitlab.com/srcgo/gopt/gopt"
)

func main() {
	client := gopt.NewClient(
		os.Getenv(gopt.ApiKeyEnvVar),
		os.Getenv(gopt.OrganizationIdEnvVar))

	// Test messages whose embedding vectors will be determined below
	messages := []string{
		"Cars are mostly expensive.",
		"Roses are usually red.",
		"Numbers are infinite.",
	}

	messageVectors := []gopt.EmbeddingVector{}

	for _, message := range messages {
		it := client.CreateEmbedding(message)

		if next, hasNext := it.Next(); hasNext {
			messageVectors = append(messageVectors, next.Data[0].Embedding)
		}

		exitOnError(it.Error())
	}

	// Example request whose embedding vector is matched against the vectors of the
	// test messages to find the message with the closest match.
	request := "What colors are roses?"
	it := client.CreateEmbedding(request)

	best := struct {
		index int
		value float64
	}{-1, -2}

	if next, hasNext := it.Next(); hasNext {
		for i, vector := range messageVectors {
			s := next.Data[0].Embedding.CosineSimilarity(vector)

			if s > best.value {
				best = struct {
					index int
					value float64
				}{i, s}
			}
		}

		fmt.Println(messages[best.index])
	}

	exitOnError(it.Error())
}

func exitOnError(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
