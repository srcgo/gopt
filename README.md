# gopt

Communicate with [ChatGPT](https://openai.com/blog/chatgpt/) directly from the command line.

## Install

```sh
go install gitlab.com/srcgo/gopt/cmd/gopt@latest
```

## Download

You can also find prebuild binaries [here](https://gitlab.com/srcgo/gopt/-/releases).

## Usage

Following the output of `gopt -help`:

```text
Description:
  Interact with the OpenAI API (https://openai.com/api/) from the command line. An
  API key needs to be provided with the environment variable OPENAI_API_KEY. An
  optional organization id may be set with OPENAI_ORGANIZATION_ID. Input can be
  provided by piping it into the program and/or as additional arguments. Any piped
  input will be appended to input provided as arguments, separated by a newline
  character. When run as interactive chat, the initial input is sent immediately
  without being displayed in the chatlog.

Usage:
  -chat
        Run as interactive chat (implies completion mode)
  -mode string
        Mode of operation (completion|edit|image|image-edit|image-variation|
        embedding) (default "completion")
```

## Library

The gopt library defines a client data structure for easy access to the [OpenAI API](https://beta.openai.com/docs/introduction).

### Example

```go
func main() {
    client := gopt.NewClient(
        os.Getenv(gopt.ApiKeyEnvVar),
        os.Getenv(gopt.OrganizationIdEnvVar))

    request := "Give me two paragraphs."
    err := client.WriteCompletion(request, os.Stdout)

    if err != nil {
        fmt.Fprintln(os.Stderr, err.Error())
        os.Exit(1)
    }
}
```

> Check out the [OpenAI API documentation](https://beta.openai.com/docs/) for more information about possible options for API endpoints and response formats.

You can find more examples [here](/examples/).

## Further Notes

As shown in the [example](#example) an API key must be provided to initialize a client - in this case read from an environment variable. You can generate a key in your [OpenAI Account](https://beta.openai.com/account/api-keys).

You may consider exporting the key into your environment on startup of your system (e.g. by placing the `export` command in `.bashrc`). If security is a concern another possibility is to store the key in encrypted form and only decrypt it on demand.

For example by using this [`keystore`](https://gitlab.com/-/snippets/2490602) script the key will only remain in memory as long as the terminal is kept open:

```sh
export OPENAI_API_KEY=$(keystore ~/.openai.key)
```

> Executing `keystore` prompts the user for a password.

## Versioning

This project uses semantic versioning. Any version below v1.0.0 is considered to be in pre-release state.
